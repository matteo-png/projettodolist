<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\TacheController;
use \App\Http\Controllers\TagController;
use \App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/home', function () {
//    return view('taches/kanban');
//});

//Route::get('/modale',function(){
//    return view('taches/modale/show');
//});


Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';

//routes pour les taches
Route::resource('taches',TacheController::class);
Route::get('/taches/destroy/{id}',[TacheController::class, 'destroy'])->name('posts.delete');

//routes pour les tags
Route::resource('tags', TagController::class);
Route::get('/tags/destroy/{id}',[TagController::class, 'destroy'])->name('tags.delete');

//routes pour les users
Route::resource('users', UserController::class);



