<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/taches" class="brand-link">
        <img src="./../adminlte/img/logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">ToDo Forge</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="../adminlte/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="/profile" class="d-block">{{ Auth::user()->name }}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->

                <li class="nav-header">Liens :</li>
                <li class="nav-item">
                    <a href="/taches" class="nav-link active">
                        <i class="nav-icon fas fa-columns"></i>
                        <p>
                            ToDo Board
                        </p>
                    </a>
                    <a href="/tags" class="nav-link active">
                        <i class="nav-icon fas fa-table"></i>
                        <p>
                            Liste des tags
                        </p>
                    </a>
                    @if(Auth::user()->isAdmin == true)
                        <a href="/users" class="nav-link active">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Liste des users
                            </p>
                        </a>
                    @endif

                </li>


            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
