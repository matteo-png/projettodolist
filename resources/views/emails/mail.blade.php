<p>Bonjour {{$user->name}},</p>

<p>Ceci est un rappel que votre tâche "{{ $tache->title }}" qui est 'En Attente' doit être terminée avant le {{ $tache->deadline }}.</p>

<p>Cordialement,</p>
<p>Votre application de gestion de tâches</p>
