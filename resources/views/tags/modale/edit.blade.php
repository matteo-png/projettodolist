<!-- Modal -->
<div class="modal fade" id="EditTache" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modifier le tag : {{$data->title}} </h5>
            </div>
            {!! Form::open(['route' => ['tags.update', $data->id], 'method' => 'PUT']) !!}
            <div class="modal-body">
                {!! Form::label('title','titre:') !!}     {!! Form::text('title', $data->title,['class'=> 'form-control']) !!}
                <br>

            </div>

            <div class="modal-footer">
                {!! Form::submit('valider',['class'=> 'btn btn-primary']) !!}
                <button type="button" class="btn btn-secondary closemodal" data-bs-dismiss="modal">Close</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<script>
    $(function(){
        $('.closemodal').click(function() {
            $('.modal').modal('hide')
        })
    })
</script>
