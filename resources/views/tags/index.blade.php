<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ToDo Forge</title>

    @include('menu/link')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
    @include('menu/navbar')

    @include('menu/aside')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper kanban">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <h1>Liste des tags :</h1>
                    </div>
                    <div class="col-sm-6 d-none d-sm-block">

                        <ol class="breadcrumb float-sm-right">
                            <span> Ajouter&nbsp;</span>
                            <button type="button" href="/tags/create" class="btn btn-success create" title="Ajout d'une tache" >
                                <i class="fa fa-solide fa-plus" ></i>
                            </button>

                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Table des tags</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0" style="height: 450px;">
                        <table class="table table-head-fixed text-nowrap">
                            <thead>
                            <tr>
                                <th style="width: 10px">ID</th>
                                <th>Title</th>
                                <th style="width: 10px">Modifier</th>
                                <th style="width: 10px">Supprimer</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($Tags as $Tag)
                            <tr>

                                <td>{{$Tag->id}}</td>
                                <td>{{$Tag->title}}</td>
                                <td><a href="/tags/{{$Tag->id}}/edit" class="btn btn-tool edit" title="Modifier la tache">
                                        <i class="fas fa-pen"></i>
                                    </a></td>
                                <td>
                                    <a href="/tags/destroy/{{$Tag->id}}" class="btn btn-tool" onclick="return confirm('Est-tu sur de vouloir le supprimer ?');" title="supprimer la tache">
                                        <i class="fas fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>

    </div>

    @include('menu/footer')

</div>
<!-- ./wrapper -->
<!-- Modal -->
<div class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div id="detail" class="modal-content">
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script>
    $(() => {
        $('.show').click(e => {
            let that = e.currentTarget;
            e.preventDefault();
            $.ajax({
                method: $(that).attr('method'),
                url: $(that).attr('href'),
                data: $(that).serialize()
            })
                .done((data) => {
                    $('#detail').html(data);
                    $('.modal').modal('show');
                })
        });
        $('.edit').click(e => {
            let that = e.currentTarget;
            e.preventDefault();
            $.ajax({
                method: $(that).attr('method'),
                url: $(that).attr('href'),
                data: $(that).serialize()
            })
                .done((data) => {
                    $('#detail').html(data);
                    $('.modal').modal('show');
                    $('#EditTache').modal({backdrop: 'static', keyboard: false});
                })
        });
        $('.create').click(e => {
            let that = e.currentTarget;
            e.preventDefault();
            $.ajax({
                method: $(that).attr('method'),
                url: $(that).attr('href'),
                data: $(that).serialize()
            })
                .done((data) => {
                    $('#detail').html(data);
                    $('.modal').modal('show');
                })
        });
    });
</script>
@include('menu/script')
</body>
</html>
