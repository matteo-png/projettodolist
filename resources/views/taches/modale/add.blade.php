<!-- Modal -->
<div class="modal fade" id="AddTache" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ajout d'une tache  </h5>
            </div>
            {!! Form::open(['route' => 'taches.store']) !!}
            <div class="modal-body">
                {!! Form::label('title','titre:') !!}     {!! Form::text('title',null,['class'=> 'form-control']) !!}
                <br>
                {!! Form::label('description','description:') !!}{!! Form::text('description',null,['class'=> 'form-control']) !!}
                <br>
                {!! Form::label('deadline','deadline:') !!}{!! Form::datetimeLocal('deadline',null,['class'=> 'form-control']) !!}
                <br>
                {!! Form::label('statut_id','statut:') !!}{!! Form::select('statut_id',$statut,null,['class'=> 'form-control']) !!}
                <br>
                {!! Form::label('tag_id[]','tag:') !!}{!! Form::select('tags_id[]',$tags,null,['multiple' => 'multiple','class'=> 'form-control']); !!}

            </div>

            <div class="modal-footer">
                {!! Form::submit('valider',['class'=> 'btn btn-primary']) !!}
                <button type="button" class="btn btn-secondary closemodal" data-bs-dismiss="modal">Close</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<script>
    $(function(){
        $('.closemodal').click(function() {
            $('.modal').modal('hide')
        })
    })
</script>
