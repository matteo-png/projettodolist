<!-- Modal -->
<div class="modal fade" id="ShowTache" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Donnée de la tache : {{$tache->title}} </h5>
            </div>
            <div class="modal-body">

                    <h5 style="color: #D35400" for="titre" class="form-control-label">Title :</h5>
                    <p  class="modal-title">{{$tache->title}}</p>
                    <h5 style="color: #D35400" for="description" class="form-control-label">Description :</h5>
                     <p  class="modal-title">{{$tache->description}}</p>
                     <h5 style="color: #D35400" for="deadline" class="form-control-label">Deadline :</h5>
                    <p  class="modal-title">{{$tache->deadline}}</p>
                    <h5 style="color: #D35400" for="created_at" class="form-control-label">Created_at :</h5>
                    <p  class="modal-title">{{$tache->created_at}}</p>
                    <h5 style="color: #D35400" for="updated_at" class="form-control-label">Updated_at :</h5>
                    <p  class="modal-title">{{$tache->updated_at}}</p>
                    <h5 style="color: #D35400" for="statut" class="form-control-label">Statut :</h5>
                    <p  class="modal-title">{{$tache->statut->title}}</p>
                    <h5 style="color: #D35400" for="statut" class="form-control-label">Attribué à :</h5>
                    <p  class="modal-title">{{$tache->user->name}}</p>
                <h5 style="color: #D35400" for="tag" class="form-control-label">Tags :</h5>
                <ul class="list-group">
                    @foreach($tache->tag as $tag)
                        <li class="list-group-item">{{$tag->title}}</li>
                    @endforeach
                </ul>



            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary closemodal" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        $('.closemodal').click(function() {
            $('.modal').modal('hide')
        })
    })
</script>
