<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>ToDo Forge</title>

  @include('menu/link')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
  @include('menu/navbar')

    @include('menu/aside')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper kanban">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <h1>ToDo Board</h1>
          </div>
          <div class="col-sm-6 d-none d-sm-block">


            <ol class="breadcrumb float-sm-right">
                <span> Ajouter&nbsp;</span>
                <button type="button" href="/taches/create" class="btn btn-success create" title="Ajout d'une tache" >
                    <i class="fa fa-solide fa-plus" ></i>
                </button>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content pb-3">
      <div class="container-fluid h-100">
        <div class="card card-row card-secondary">
          <div class="card-header">
            <h3 class="card-title">
              En attente
            </h3>
          </div>
          <div class="card-body">
              @foreach($Taches as $Tache)
                  @if ($Tache->statut->title == "En attente")
         <div class="card card-info card-outline">
              <div class="card-header">
                  <h5 class="card-title">{{$Tache->title}}</h5>
                <div class="card-tools">
                  <a href="/taches/{{$Tache->id}}" data-toggle="modal" class="btn btn-tool btn-link show" title="Voir les détails">
                      <i class="fas fa-eye"></i>
                  </a>
                    @if(Auth::user()->name == $Tache->user->name || Auth::user()->name == 'Admin')

                  <a href="/taches/{{$Tache->id}}/edit" class="btn btn-tool edit" title="Modifier la tache">
                    <i class="fas fa-pen"></i>
                  </a>
                    @endif
                    @if(Auth::user()->name == $Tache->user->name || Auth::user()->name == 'Admin')
                    <a href="/taches/destroy/{{$Tache->id}}" class="btn btn-tool" onclick="return confirm('Est-tu sur de vouloir le supprimer ?');" title="supprimer la tache">
                    <i class="fas fa-trash"></i>
                    </a>
                    @endif
                </div>
              </div>
            </div>
                  @endif
              @endforeach
          </div>
        </div>

          <!-- tableau to do -->
        <div class="card card-row card-primary">
          <div class="card-header">
            <h3 class="card-title">
              A faire
            </h3>
          </div>
          <div class="card-body">

              @foreach($Taches as $Tache)
                  @if ($Tache->statut->title == "A faire")
            <div class="card card-primary card-outline">

              <div class="card-header">

                <h5 class="card-title">{{$Tache->title}}</h5>
                <div class="card-tools">

                  <a href="/taches/{{$Tache->id}}" class="btn btn-tool btn-link show" title="Voir les détails">
                      <i class="fas fa-eye"></i>
                  </a>
                    @if(Auth::user()->name == $Tache->user->name || Auth::user()->name == 'Admin')
                  <a href="/taches/{{$Tache->id}}/edit" class="btn btn-tool edit" title="Modifier la tache">
                    <i class="fas fa-pen"></i>
                  </a>
                    @endif
                    @if(Auth::user()->name == $Tache->user->name || Auth::user()->name == 'Admin')
                    <a href="/taches/destroy/{{$Tache->id}}" class="btn btn-tool" onclick="return confirm('Est-tu sur de vouloir le supprimer ?');" title="supprimer la tache">
                        <i class="fas fa-trash"></i>
                    </a>
                    @endif
                </div>
              </div>
            </div>
                  @endif
              @endforeach

          </div>

        </div>
          <!-- Fin to do -->

          <!-- Tableau In progress -->
        <div class="card card-row card-default">
          <div class="card-header bg-info">
            <h3 class="card-title">
              En cours
            </h3>
          </div>
          <div class="card-body">
              @foreach($Taches as $Tache)
                  @if ($Tache->statut->title == "En cours")
            <div class="card card-info card-outline">
              <div class="card-header">
                  <h5 class="card-title">{{$Tache->title}}</h5>
                <div class="card-tools">
                  <a href="/taches/{{$Tache->id}}" class="btn btn-tool btn-link show" title="Voir les détails">
                      <i class="fas fa-eye"></i>
                  </a>
                    @if(Auth::user()->name == $Tache->user->name || Auth::user()->name == 'Admin')
                  <a href="/taches/{{$Tache->id}}/edit" class="btn btn-tool edit" title="Modifier la tache">
                    <i class="fas fa-pen"></i>
                  </a>
                    @endif
                    @if(Auth::user()->name == $Tache->user->name || Auth::user()->name == 'Admin')
                    <a href="/taches/destroy/{{$Tache->id}}" class="btn btn-tool" onclick="return confirm('Est-tu sur de vouloir le supprimer ?');" title="supprimer la tache">
                        <i class="fas fa-trash"></i>
                    </a>
                    @endif
                </div>
              </div>
            </div>
                  @endif
              @endforeach
          </div>
        </div>
          <!-- Fin In progress -->
          <!-- Tableau Terminé -->
        <div class="card card-row card-success">
          <div class="card-header">
            <h3 class="card-title">
              Terminé
            </h3>
          </div>
          <div class="card-body">
              @foreach($Taches as $Tache)
                  @if ($Tache->statut->title == "Terminé")
            <div class="card card-success card-outline">
              <div class="card-header">
                  <h5 class="card-title">{{$Tache->title}}</h5>
                <div class="card-tools">
                  <a href="/taches/{{$Tache->id}}" class="btn btn-tool btn-link show" title="Voir les détails">
                      <i class="fas fa-eye"></i>
                  </a>
                    @if(Auth::user()->name == $Tache->user->name || Auth::user()->name == 'Admin')
                  <a href="/taches/{{$Tache->id}}/edit" class="btn btn-tool edit" title="Modifier la tache">
                    <i class="fas fa-pen"></i>
                  </a>
                    @endif
                    @if(Auth::user()->name == $Tache->user->name || Auth::user()->name == 'Admin')
                    <a href="/taches/destroy/{{$Tache->id}}" class="btn btn-tool" onclick="return confirm('Est-tu sur de vouloir le supprimer ?');" title="supprimer la tache">
                        <i class="fas fa-trash"></i>
                    </a>
                    @endif
                </div>
              </div>
            </div>
                  @endif
              @endforeach
          </div>
        </div>
          <!-- Fin Terminé -->
      </div>
    </section>
  </div>

    @include('menu/footer')
</div>
<!-- ./wrapper -->
<!-- Modal -->
<div class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div id="detail" class="modal-content">
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script>
    $(() => {
        $('.show').click(e => {
            let that = e.currentTarget;
            e.preventDefault();
            $.ajax({
                method: $(that).attr('method'),
                url: $(that).attr('href'),
                data: $(that).serialize()
            })
                .done((data) => {
                    $('#detail').html(data);
                    $('.modal').modal('show');
                })
        });
        $('.edit').click(e => {
            let that = e.currentTarget;
            e.preventDefault();
            $.ajax({
                method: $(that).attr('method'),
                url: $(that).attr('href'),
                data: $(that).serialize()
            })
                .done((data) => {
                    $('#detail').html(data);
                    $('.modal').modal('show');
                    $('#EditTache').modal({backdrop: 'static', keyboard: false});
                })
        });
        $('.create').click(e => {
            let that = e.currentTarget;
            e.preventDefault();
            $.ajax({
                method: $(that).attr('method'),
                url: $(that).attr('href'),
                data: $(that).serialize()
            })
                .done((data) => {
                    $('#detail').html(data);
                    $('.modal').modal('show');
                })
        });
    });
</script>
@include('menu/script')

</body>
</html>
