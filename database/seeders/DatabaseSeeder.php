<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Statut;
use App\Models\Tache;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        User::firstOrcreate([
            'name' =>'Admin',
            'email' => 'admin@todoForge.fr',
            'password' => bcrypt('admin123'),
            'isAdmin' => true,
        ]);
        $users = User::factory(4)->create();

        $tags = Tag::factory(10)->create();

        $statuts = Statut::factory()->createMany([
            ['title'=>'En attente'],
            ['title'=>'A faire'],
            ['title'=>'En cours'],
            ['title'=>'Terminé']
        ]);

        Tache::factory(10)->create()->each(function ($tache) use ($users, $tags, $statuts) {
            $tache->user()->associate($users->random());
            $tache->statut()->associate($statuts->random());
            $tache->save();

            $tache->tag()->attach($tags->random(rand(1, 5)));
        });
    }
}
