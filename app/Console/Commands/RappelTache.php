<?php

namespace App\Console\Commands;

use App\Mail\MailTache;
use App\Models\Statut;
use App\Models\Tache;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class RappelTache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rappel:tache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

       $taches = DB::table('taches')->whereDate('deadline', Carbon::today()->addDays(3))->get();

        foreach ($taches as $tache) {
            $statut = Statut::findOrFail($tache->statut_id);
            if($statut->title == "En attente"){
                $user = User::findOrFail($tache->user_id);
                Mail::to($user->email)->send(new MailTache($tache,$user));
            }

        }

    }
}

