<?php

namespace App\Listeners;

use App\Events\NotifyTache;
use App\Jobs\TacheJob;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotifierTache
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\NotifyTache  $event
     * @return void
     */
    public function handle(NotifyTache $event)
    {
        TacheJob::dispatch($event->tache);
    }
}
