<?php

namespace App\Http\Controllers;

use App\Events\NotifyTache;
use App\Models\Statut;
use App\Models\Tache;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TacheController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Taches = Tache::with(['statut:title,id','user:name,id','tag:title'])->get(['id','title','description','deadline','created_at','updated_at','statut_id','user_id']);
        return view('taches/kanban',['Taches'=>$Taches]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $statut = Statut::all('id','title')->pluck('title','id');
        $tags = Tag::all('id','title')->pluck('title','id');
        return view('taches/modale/add',['statut' => $statut,'tags' => $tags]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();

        $requestData = $request->all();
        $requestData['user_id'] = $user->id;

        $tache = Tache::create($requestData);

        $tache->tag()->sync($request->tags_id);

        return redirect()->route('taches.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('taches/modale/show',['tache'=> Tache::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Tache::findOrFail($id);
         $data->load('tag');
         $tags = Tag::all('title','id')->pluck('title','id');


        return view('taches/modale/edit', ['data'=>$data,'tags'=>$tags]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $tache = Tache::findOrFail($id);

        $tache->update($request->all());

        $tags = $request->input('tags_id', []);

        $tache->tag()->sync($tags);

        return redirect()->route('taches.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $tache = Tache::findOrFail($id);
        $tache->tag()->detach();

        $tache->delete();
        return redirect()->route('taches.index');
    }
}
